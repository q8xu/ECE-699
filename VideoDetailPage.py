import os

import cv2
from PyQt5.QtCore import QUrl, Qt, QRectF
from PyQt5.QtGui import QImage, QPixmap, QPen, QColor, QBrush, QPainter
from PyQt5.QtMultimedia import QMediaPlayer, QMediaContent
from PyQt5.QtMultimediaWidgets import QVideoWidget
from PyQt5.QtWidgets import QWidget, QVBoxLayout, QLabel, QPushButton, QSlider, QStyleOptionSlider, QStyle, QDialog, \
    QGraphicsScene, QGraphicsView, QInputDialog


class VideoDetailPage(QWidget):
    def __init__(self, item_detail):
        super().__init__()

        self.setWindowTitle('Video Detail Page')
        # self.showMaximized()  # This line will set the window to full screen
        self.resize(640, 480)

        self.layout = QVBoxLayout()

        time_action_text = f"{item_detail['time']} {item_detail['action']}"
        self.description_label = QLabel(time_action_text)
        self.layout.addWidget(self.description_label)

        self.video_widget = QVideoWidget()
        self.layout.addWidget(self.video_widget, 1)

        self.media_player = QMediaPlayer()
        video_path = item_detail["video_path"]
        self.media_player.setMedia(QMediaContent(QUrl.fromLocalFile(video_path)))
        self.media_player.setVideoOutput(self.video_widget)
        self.media_player.error.connect(self.handle_error)

        # Maintain the aspect ratio
        self.video_widget.setAspectRatioMode(Qt.KeepAspectRatio)

        # Slider
        self.slider = ClickableSlider(Qt.Horizontal)
        self.layout.addWidget(self.slider)

        # Update the slider's position when the media player's position changes
        self.media_player.positionChanged.connect(self.slider.setValue)

        # Seek to the slider's position when the user moves the slider
        self.slider.sliderMoved.connect(self.media_player.setPosition)

        # Update the slider's maximum value when the media player's duration changes
        self.media_player.durationChanged.connect(self.slider.setMaximum)

        # Buttons
        self.play_button = QPushButton("Play")
        self.pause_button = QPushButton("Pause")

        self.layout.addWidget(self.play_button)
        self.layout.addWidget(self.pause_button)

        if item_detail.get("need_separate", False):
            self.separate_button = QPushButton("Separate")
            self.layout.addWidget(self.separate_button)
            self.separate_button.clicked.connect(self.handle_separate_click)

        self.setLayout(self.layout)

        self.play_button.clicked.connect(self.media_player.play)
        self.pause_button.clicked.connect(self.media_player.pause)

    def handle_error(self):
        print("Error: " + self.media_player.errorString())

    def handle_separate_click(self):
        thumbnail_path = self.generate_thumbnail(self.media_player.media().canonicalUrl().toLocalFile())
        if thumbnail_path:
            dialog = RectangleDrawingDialog(thumbnail_path)
            dialog.exec_()

    @staticmethod
    def generate_thumbnail(video_path):
        THUMBNAILS_DIR = './data/separate/'
        if not os.path.exists(THUMBNAILS_DIR):
            os.makedirs(THUMBNAILS_DIR)

        video_cap = cv2.VideoCapture(video_path)

        # Check if video opened successfully
        if not video_cap.isOpened():
            print(f"Error opening video file {video_path}")
            return None

        # Read the first frame
        ret, frame = video_cap.read()
        thumbnail_path = None
        if ret:
            video_name = os.path.basename(video_path)
            thumbnail_path = os.path.join(THUMBNAILS_DIR, video_name.replace('.mp4', '.png'))
            # Write the thumbnail
            cv2.imwrite(thumbnail_path, frame)
        else:
            print(f"Error reading video file {video_path}")

        video_cap.release()
        return thumbnail_path


class ClickableSlider(QSlider):
    def mousePressEvent(self, event):
        QSlider.mousePressEvent(self, event)
        if event.button() == Qt.LeftButton:
            value = self.minimum() + ((self.maximum() - self.minimum()) * event.x()) / self.width()
            self.setValue(value)
            event.accept()

    def pixelPosToRangeValue(self, pos):
        opt = QStyleOptionSlider()
        self.initStyleOption(opt)
        gr = self.style().subControlRect(QStyle.CC_Slider, opt, QStyle.SC_SliderGroove, self)
        sr = self.style().subControlRect(QStyle.CC_Slider, opt, QStyle.SC_SliderHandle, self)

        if self.orientation() == Qt.Horizontal:
            sliderLength = sr.width()
            sliderMin = gr.x()
            sliderMax = gr.right() - sliderLength + 1
        else:
            sliderLength = sr.height()
            sliderMin = gr.y()
            sliderMax = gr.bottom() - sliderLength + 1
        pr = pos - sr.center() + sr.topLeft()
        p = pr.x() if self.orientation() == Qt.Horizontal else pr.y()
        return QStyle.sliderValueFromPosition(self.minimum(), self.maximum(), p - sliderMin,
                                              sliderMax - sliderMin, opt.upsideDown)


class RectangleDrawingView(QGraphicsView):
    def __init__(self, scene, parent=None):
        super().__init__(scene, parent)
        self.start_point = None

    def mousePressEvent(self, event):
        self.start_point = self.mapToScene(event.pos())

    def mouseReleaseEvent(self, event):
        if self.start_point:
            end_point = self.mapToScene(event.pos())
            rect_item = self.scene().addRect(QRectF(self.start_point, end_point), QPen(QColor(255, 0, 0)),
                                             QBrush(QColor(255, 0, 0, 40)))
            label, ok = QInputDialog.getText(self, 'Label the rectangle', 'Enter label:')
            if ok:
                self.scene().addText(label).setPos(end_point)
                self.parent().rectangles.append((rect_item, label))
            self.start_point = None


class RectangleDrawingDialog(QDialog):
    def __init__(self, image_path):
        super().__init__()
        self.rectangles = []

        self.scene = QGraphicsScene()
        self.view = RectangleDrawingView(self.scene, self)
        self.view.setRenderHint(QPainter.Antialiasing)
        self.view.setOptimizationFlags(QGraphicsView.DontAdjustForAntialiasing | QGraphicsView.DontSavePainterState)
        self.view.setViewportUpdateMode(QGraphicsView.FullViewportUpdate)

        layout = QVBoxLayout()
        layout.addWidget(self.view)

        self.save_button = QPushButton("Save")
        self.save_button.clicked.connect(self.save_rectangles)
        layout.addWidget(self.save_button)

        self.setLayout(layout)

        self.image = QImage(image_path)
        pixmap = QPixmap.fromImage(self.image)
        pixmap_item = self.scene.addPixmap(pixmap)

        self.showMaximized()

        self.view.setSceneRect(pixmap_item.boundingRect())
        self.view.fitInView(pixmap_item, Qt.KeepAspectRatio)

    def save_rectangles(self):
        with open("separate.txt", "w") as f:
            for rect, label in self.rectangles:
                bounding_rect = rect.rect()
                start = bounding_rect.topLeft()
                end = bounding_rect.bottomRight()
                f.write(f"{start.x()} {end.x()} {start.y()} {end.y()} {label}\n")
        self.accept()
