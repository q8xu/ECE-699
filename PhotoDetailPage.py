from PyQt5.QtCore import Qt
from PyQt5.QtGui import QImage, QPixmap
from PyQt5.QtWidgets import QWidget, QVBoxLayout, QLabel, QGraphicsView, QGraphicsScene, QGraphicsPixmapItem, \
    QPushButton


class PhotoDetailPage(QWidget):
    def __init__(self, item_detail):
        super().__init__()

        self.setWindowTitle('Photo Detail Page')
        self.showMaximized()

        self.layout = QVBoxLayout()

        time_action_text = f"{item_detail['time']} {item_detail['action']}"
        self.description_label = QLabel(time_action_text)
        self.layout.addWidget(self.description_label)

        # Create QGraphicsView and QGraphicsScene
        self.view = QGraphicsView()
        self.scene = QGraphicsScene()
        self.view.setScene(self.scene)

        # Load image
        image = QImage(item_detail['img'])
        pixmap = QPixmap.fromImage(image)

        # Resize pixmap to desired initial size
        view_width = self.view.size().width()
        view_height = self.view.size().height()
        pixmap = pixmap.scaled(view_width, view_height, Qt.KeepAspectRatio)

        # Add the pixmap to a QGraphicsPixmapItem
        self.pixmap_item = QGraphicsPixmapItem(pixmap)

        self.scene.addItem(self.pixmap_item)

        self.zoom_in_button = QPushButton("Zoom In")
        self.zoom_in_button.clicked.connect(self.zoom_in)

        self.zoom_out_button = QPushButton("Zoom Out")
        self.zoom_out_button.clicked.connect(self.zoom_out)

        self.layout.addWidget(self.view)
        self.layout.addWidget(self.zoom_in_button)
        self.layout.addWidget(self.zoom_out_button)

        self.setLayout(self.layout)

    def zoom_in(self):
        self.view.scale(1.25, 1.25)  # 25% larger

    def zoom_out(self):
        self.view.scale(0.8, 0.8)  # 20% smaller
