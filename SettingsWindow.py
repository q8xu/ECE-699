import os

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QVBoxLayout, QWidget, QListWidget, QPushButton, QListWidgetItem, QDialog


class SettingsWindow(QDialog):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.setModal(True)

        self.setWindowTitle("Settings")
        self.layout = QVBoxLayout()

        self.list_widget = QListWidget()
        self.layout.addWidget(self.list_widget)

        self.ok_button = QPushButton("OK")
        self.ok_button.clicked.connect(self.save_objects)
        self.layout.addWidget(self.ok_button)

        self.setLayout(self.layout)

        self.resize(300, 400)

        self.load_objects()

    def load_objects(self):
        # Load all objects from 'all_object.txt'
        with open('all_object.txt', 'r') as f:
            self.all_objects = [line.strip() for line in f]

        for obj in self.all_objects:
            item = QListWidgetItem(obj)
            item.setFlags(item.flags() | Qt.ItemIsUserCheckable)
            item.setCheckState(Qt.Checked if obj in self.read_tracked_objects() else Qt.Unchecked)
            self.list_widget.addItem(item)

    def save_objects(self):
        with open('track_object.txt', 'w') as f:
            for index in range(self.list_widget.count()):
                item = self.list_widget.item(index)
                if item.checkState() == Qt.Checked:
                    f.write(item.text() + '\n')
        self.close()

    def read_tracked_objects(self):
        if os.path.exists('track_object.txt'):
            with open('track_object.txt', 'r') as f:
                return [line.strip() for line in f]
        return []
