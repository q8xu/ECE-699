import os
import re
from datetime import datetime

from PyQt5.QtCore import Qt
from PyQt5.QtGui import QPixmap, QFont
from PyQt5.QtWidgets import QWidget, QVBoxLayout, QLabel, QListWidget, QListWidgetItem, QHBoxLayout

from PhotoDetailPage import PhotoDetailPage
from VideoDetailPage import VideoDetailPage


class DetailPage(QWidget):
    def __init__(self, item_name, directory):
        super().__init__()

        self.setWindowTitle('Detail Page')
        self.showMaximized()  # This line will set the window to full screen

        self.layout = QVBoxLayout()

        self.status_label = QLabel("Latest Status:")
        self.layout.addWidget(self.status_label)

        # Fetch the data for the item
        item_detail = self.fetch_item_detail(item_name, directory)

        # Convert the latest status into a single-item list
        self.latest_status_list_widget = QListWidget()
        latest_status_item = QListWidgetItem(self.latest_status_list_widget)
        # is_video = latest_status_item.get("is_video", False)
        latest_status_widget = ItemWidget(item_detail["img"], item_detail["video"], item_detail["action"],
                                          item_detail["time"], img_size=200, action_font_size=24, time_font_size=22,
                                          is_video=item_detail["is_video"], need_separate=item_detail["need_separate"])
        latest_status_item.setSizeHint(latest_status_widget.sizeHint())

        self.latest_status_list_widget.addItem(latest_status_item)
        self.latest_status_list_widget.setItemWidget(latest_status_item, latest_status_widget)

        self.layout.addWidget(self.latest_status_list_widget)

        self.history_label = QLabel("History:")
        self.layout.addWidget(self.history_label)

        # Create the list for history
        self.history_list_widget = QListWidget()
        self.layout.addWidget(self.history_list_widget)

        for history_item in item_detail['history']:
            list_item = QListWidgetItem(self.history_list_widget)
            # is_video = history_item.get("is_video", False)
            history_item_widget = ItemWidget(history_item["img"], history_item["video"], history_item["action"],
                                             history_item["time"], img_size=50, action_font_size=16, time_font_size=14,
                                             is_video=history_item["is_video"],
                                             need_separate=history_item["need_separate"])
            list_item.setSizeHint(history_item_widget.sizeHint())

            self.history_list_widget.addItem(list_item)
            self.history_list_widget.setItemWidget(list_item, history_item_widget)

        self.setLayout(self.layout)

        # connect the signal when an item in the history list or latest status is clicked
        self.history_list_widget.itemClicked.connect(self.show_detailed_page)
        self.latest_status_list_widget.itemClicked.connect(self.show_detailed_page)

    def show_detailed_page(self, item):
        list_widget = self.sender()
        item_widget = list_widget.itemWidget(item)
        if item_widget.is_video:
            self.video_detail_page = VideoDetailPage(
                {"video_path": item_widget.video_path,
                 "time": item_widget.time_label.text(),
                 "action": item_widget.action_label.text(),
                 "need_separate": item_widget.need_separate})
            self.video_detail_page.show()
        else:
            self.image_detail_page = PhotoDetailPage(
                {"img": item_widget.img_path,
                 "time": item_widget.time_label.text(),
                 "action": item_widget.action_label.text()})
            self.image_detail_page.show()

    def fetch_item_detail(self, item_name, directory):
        latest_video = None
        history = []
        thumbnail_directory = './data/thumbnail/'

        for root, dirs, files in os.walk(directory):
            for file in files:
                if file.endswith(".mp4"):
                    match = re.match(fr"(\d{{2}}:\d{{2}}:\d{{2}}) {item_name} id:\d+ (left|enter) H\.mp4", file)
                    if match:
                        time_str, action = match.groups()
                        time = datetime.strptime(time_str, "%H:%M:%S")  # convert the time string into a datetime object

                        video_path = os.path.abspath(
                            os.path.join(root, file))  # get the absolute full path of the video
                        img_path = os.path.join(thumbnail_directory,
                                                file.replace(".mp4", ".png"))  # get the full path of the thumbnail

                        video_detail = {
                            "img": img_path,
                            "video": video_path,
                            "time": time_str,
                            "action": action,
                            "is_video": True,  # as this is a video file, 'is_video' is set to True
                            "need_separate": False,
                        }

                        # Set video '23:27:08 keyboard id:21 left H.mp4' as video that need to be labeled.
                        if file == '23:27:08 keyboard id:21 left H.mp4':
                            video_detail["need_separate"] = True

                        if latest_video is None or time > datetime.strptime(latest_video["time"], "%H:%M:%S"):
                            if latest_video is not None:
                                history.append(latest_video)  # append the previous latest video to history
                            latest_video = video_detail
                        else:
                            history.append(video_detail)

        if item_name == "chair":
            img_detail = {
                "img": "./data/separate.png",
                "video": "./data/separate.png",
                "time": "23:27:10",
                "action": "enter",
                "is_video": False,  # as this is a img file, 'is_video' is set to False
                "need_separate": False,
            }
            history.append(img_detail)

        history.reverse()
        item_detail = latest_video
        item_detail["history"] = history

        return item_detail


class ItemWidget(QWidget):
    def __init__(self, img_path, video_path, action, time, img_size=50, action_font_size=12, time_font_size=10,
                 is_video=False, need_separate=False):
        super().__init__()

        self.layout = QHBoxLayout()
        self.is_video = is_video
        self.need_separate = need_separate
        self.video_path = video_path
        self.img_path = img_path

        self.image_label = QLabel()
        self.image = QPixmap(img_path)
        self.image = self.image.scaled(img_size, img_size, Qt.IgnoreAspectRatio)
        self.image_label.setPixmap(self.image)
        self.layout.addWidget(self.image_label)

        self.text_layout = QVBoxLayout()

        self.action_label = QLabel(action)
        action_font = QFont()
        action_font.setPointSize(action_font_size)
        self.action_label.setFont(action_font)

        self.time_label = QLabel(time)
        time_font = QFont()
        time_font.setPointSize(time_font_size)
        self.time_label.setFont(time_font)

        self.text_layout.addWidget(self.action_label)
        self.text_layout.addWidget(self.time_label)

        # Add the separate reminder if need_separate is True
        if need_separate:
            self.reminder_label = QLabel(
                "*This video needs to decide if objects need to be separated.\nClick for details.")
            reminder_font = QFont()
            reminder_font.setPointSize(time_font_size)
            self.reminder_label.setFont(reminder_font)
            self.reminder_label.setStyleSheet("color: red;")
            self.text_layout.addWidget(self.reminder_label)

        self.text_layout.addStretch()  # Add a spacer at the bottom

        self.layout.addLayout(self.text_layout)

        self.setLayout(self.layout)
