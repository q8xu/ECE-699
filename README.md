# ECE-699  Memory Visualization for an Assistive Robot

## Dependencies
- **Python**: Version 3.9
- **PyQt**: Version 5.9.2
- **OpenCV (cv2)**: Version 4.8.0

## Usage
1. Clone the repo to your local directory
2. If there are new videos, run the `GenerateThumbnail.py` script. If not, skip.
3. Run `main.py` to start the GUI.

## GUI Architecture

Here's the architecture of the project:

![Project Architecture](GUI_architecture.png)

## Code Structure

- `main.py`: Run to start the main page.
- `SettingsWindow.py`: The settings window in the lower left corner of the main page. Used to set which items the robot tracks.
- `DetailPage.py`: Object detail page.
- `PhotoDetailPage.py`: Photo detail page.
- `VideoDetailPage.py`: Video detail page.


- `GenerateThumbnail.py`: Script to generate thumbnails for videos. It should be run again every time the video data is updated.
- `all_object.txt`: A list of all objects that the robot can track.
- `track_object.txt`: A list of objects that the user wants the robot to track. Set in the setting window of the main page.
- `separate.txt`: Save all coordinates and labels marked by the user for video that need to be marked.
- `data/`: Save photos and videos in the robot's memory.
  - `object/`: Save a photo for each object, and it will be displayed on the main page.
  - `separate/`: Save the photos for labeling.
  - `thumbnail/`: Save the thumbnails of the videos, which is the output of the `GenerateThumbnail.py` script.
