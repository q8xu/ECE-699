import os
import cv2
import glob
import re
from PIL import Image

DATA_DIR = './data/'
THUMBNAILS_DIR = './data/thumbnail/'
PLAY_ICON_PATH = './data/play_icon.png'
VIDEO_NAME_PATTERN = r"\d{2}:\d{2}:\d{2} (.*?) id:\d+ (left|enter) H\.mp4"

# Read the play icon
play_icon = Image.open(PLAY_ICON_PATH)

# Go through all mp4 files in the data directory
for video_path in glob.glob(os.path.join(DATA_DIR, '*.mp4')):
    video_name = os.path.basename(video_path)

    # Check if the video name matches the pattern
    if not re.match(VIDEO_NAME_PATTERN, video_name):
        continue

    video_cap = cv2.VideoCapture(video_path)

    # Check if video opened successfully
    if not video_cap.isOpened():
        print(f"Error opening video file {video_path}")
        continue

    # Read the first frame
    ret, frame = video_cap.read()
    if ret:
        thumbnail_path = os.path.join(THUMBNAILS_DIR, video_name.replace('.mp4', '.png'))

        # Write the thumbnail
        cv2.imwrite(thumbnail_path, frame)

        # Open the thumbnail with PIL
        thumbnail = Image.open(thumbnail_path)

        # Compute the coordinates for centering the play icon
        width, height = thumbnail.size
        icon_width, icon_height = play_icon.size
        x = (width - icon_width) // 2
        y = (height - icon_height) // 2

        # Add the play icon
        thumbnail.paste(play_icon, (x, y), play_icon)

        # Save the thumbnail
        thumbnail.save(thumbnail_path)
    else:
        print(f"Error reading video file {video_path}")

    video_cap.release()
