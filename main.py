import os
import re

from PyQt5.QtWidgets import QApplication, QMainWindow, QWidget, QVBoxLayout, QHBoxLayout, QListWidget, QListWidgetItem, \
    QLabel, QComboBox, QPushButton, QLineEdit
from PyQt5.QtGui import QPixmap, QFont
from PyQt5.QtCore import Qt

from DetailPage import DetailPage
from SettingsWindow import SettingsWindow


class ItemWidget(QWidget):
    def __init__(self, img_path, name, description, img_size=50, name_font_size=12, description_font_size=10,
                 is_video=False):
        super().__init__()

        self.layout = QHBoxLayout()
        self.is_video = is_video

        self.image_label = QLabel()
        self.image = QPixmap(img_path)
        self.image = self.image.scaled(img_size, img_size, Qt.IgnoreAspectRatio)
        self.image_label.setPixmap(self.image)
        self.layout.addWidget(self.image_label)

        self.text_layout = QVBoxLayout()

        self.name_label = QLabel(name)
        name_font = QFont()
        name_font.setPointSize(name_font_size)
        self.name_label.setFont(name_font)

        self.description_label = QLabel(description)
        description_font = QFont()
        description_font.setPointSize(description_font_size)
        self.description_label.setFont(description_font)

        self.text_layout.addWidget(self.name_label)
        self.text_layout.addWidget(self.description_label)

        self.text_layout.addStretch()  # Add a spacer at the bottom

        self.layout.addLayout(self.text_layout)

        self.setLayout(self.layout)


class CustomWidget(QWidget):
    def __init__(self):
        super().__init__()

        self.layout = QHBoxLayout()

        self.left_widget = QWidget()
        self.left_layout = QVBoxLayout()
        self.left_widget.setLayout(self.left_layout)

        self.right_widget = QWidget()
        self.right_layout = QVBoxLayout()
        self.right_widget.setLayout(self.right_layout)

        self.search_bar = QLineEdit()
        self.search_bar.setPlaceholderText("Search by name")

        self.category_label = QLabel("Category:")
        self.select_bar1 = QComboBox()
        self.select_bar1.addItem("none")
        self.select_bar1.addItem("object")
        self.select_bar1.addItem("event")
        self.select_bar1.addItem("interaction")

        self.place_label = QLabel("Place:")
        self.select_bar2 = QComboBox()
        self.select_bar2.addItem("none")
        self.select_bar2.addItem("kitchen")
        self.select_bar2.addItem("living room")

        self.search_button = QPushButton("Search")
        self.search_button.clicked.connect(self.filter_list)

        self.left_layout.addWidget(self.search_bar)
        self.left_layout.addWidget(self.category_label)
        self.left_layout.addWidget(self.select_bar1)
        self.left_layout.addWidget(self.place_label)
        self.left_layout.addWidget(self.select_bar2)
        self.left_layout.addWidget(self.search_button)

        self.settings_button = QPushButton("Settings")
        self.settings_button.clicked.connect(self.open_settings)

        # Add a stretch at the bottom. This will expand to fill the remaining space,
        # pushing all the other widgets to the top half.
        self.left_layout.addStretch()

        self.left_layout.addWidget(self.settings_button)

        self.list_widget = QListWidget()
        self.list_widget.itemClicked.connect(self.open_detail_page)

        self.right_layout.addWidget(self.list_widget)

        # self.items = [
        #     {"img": "./data/apple", "name": "apple", "description": "on the kitchen table"},
        #     {"img": "./data/phone", "name": "phone", "description": "on the living room table"},
        # ]
        self.items = self.fetch_mp4_files("./data")

        self.filter_list()

        self.layout.addWidget(self.left_widget)
        self.layout.addWidget(self.right_widget)
        self.layout.setStretchFactor(self.left_layout, 1)
        self.layout.setStretchFactor(self.right_layout, 3)

        self.setLayout(self.layout)

    def filter_list(self):
        name = self.search_bar.text()
        place = self.select_bar2.currentText()

        # Reset the list
        self.list_widget.clear()

        for item in self.items:
            if (name in item["name"] or not name) and (place in item["description"] or place == "none"):
                list_item = QListWidgetItem(self.list_widget)
                item_widget = ItemWidget(item["img"], item["name"], item["description"], img_size=50, name_font_size=16,
                                         description_font_size=14)
                list_item.setSizeHint(item_widget.sizeHint())

                self.list_widget.addItem(list_item)
                self.list_widget.setItemWidget(list_item, item_widget)

    def open_detail_page(self, item):
        # item_widget = self.list_widget.itemWidget(item)
        item_detail = {
            'img': './data/apple',
            'action': 'Seen',
            'time': '2022-07-26 04:30 pm',
            'history': [
                {'img': './data/apple', 'action': 'Seen', 'time': '2022-07-06 04:30 pm'},
                {'img': './data/apple_video', 'action': 'Disappeared', 'time': '2022-07-06 04:31 pm', 'is_video': True},
            ]
        }
        #
        # self.detail_page = DetailPage(item_detail)
        # self.detail_page.show()
        item_widget = self.list_widget.itemWidget(item)
        item_name = item_widget.name_label.text()

        self.detail_page = DetailPage(item_name, "./data")
        self.detail_page.show()

    def open_settings(self):
        self.settings_window = SettingsWindow(self)
        self.settings_window.show()

    def fetch_mp4_files(self, directory):
        items = []
        added_names = set()  # to keep track of what we've already added

        # Traverse directory
        for root, dirs, files in os.walk(directory):
            for file in files:
                if file.endswith(".mp4"):
                    # Use a regular expression to check if the file follows the format
                    if re.match(r"\d{2}:\d{2}:\d{2} (.*?) id:\d+ (left|enter) H\.mp4", file):
                        name = file.split()[1]  # get the object's name
                        if name not in added_names:  # if we haven't already added this name
                            items.append({
                                "img": os.path.join("./data", "object", name),  # path to the image
                                "name": name,
                                "description": "",  # description is empty initially
                            })
                            added_names.add(name)  # mark this name as added

        return items


class AppWindow(QMainWindow):
    def __init__(self):
        super().__init__()

        # Set window title
        self.setWindowTitle("My App")

        # Set central widget
        self.central_widget = CustomWidget()
        self.setCentralWidget(self.central_widget)

        # Maximize the window
        self.showMaximized()


if __name__ == "__main__":
    app = QApplication([])

    window = AppWindow()
    window.show()

    app.exec_()
